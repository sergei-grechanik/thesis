% !TEX root = grechanik-summary-en.tex

\newcommand{\optcite}[1]{}

%\input{summary-en-title}

%\setstretch{1.15}

\begin{center}
  {\LARGE \bfseries \entitle}\\\vspace{1em}
  {\large Sergei Grechanik}
\end{center}

\section*{Introduction}

One of the claimed advantages of functional programming languages is simplicity of performing
transformations and proving program properties. This simplicity is mainly achieved by absence
of side-effects. For example, if we know that
$\forall \overline{x_i} f(\overline{x_i}) = g(\overline{x_i})$ for some functions
$f$ and $g$ then we can always replace $f(\overline{e_i})$ with $g(\overline{e_i})$ anywhere in the
program (for imperative languages this does not hold in general because of side-effects and
mutability).

There are three important groups of functional languages which differ in their treatment of
partial functions: total languages, strict languages, and
non-strict languages. In total functional languages all functions are total, in particular every
function must always terminate, and for this reason such languages cannot be Turing-complete. Total
languages are better suited for proving properties, however they are not very popular. In strict
languages all user-defined functions must be strict, i.e.\ the value of a call $f(a,b,c)$ is
undefined if any of the expressions $a$, $b$ or $c$ is undefined (e.g.\ doesn't terminate).
% Operationally strictness means that the arguments must be completely evaluated for evaluating the
% function call.
Most of programming languages are strict, however strict languages are
less suitable for program analysis and transformation. Non-strict languages allow user-defined
functions to be non-strict, i.e.\ terminate even if one the arguments doesn't terminate (the
simplest example of a non-strict function is the constant function $f(x) = C$ which doesn't use its
argument at all). The most widely-known non-strict language is Haskell. Non-strict languages are
considered better suited for program analysis and transformation than strict ones, and for this
reason in this thesis a non-strict first-order functional language was chosen as the input language.

One of the most important classes of program properties are equivalences between functions, i.e.\
properties of the form $\forall \overline{x_i} f(\overline{x_i}) = g(\overline{x_i})$. Such
equivalences can be used both for verification and optimization, and they can also be used to prove
other properties. Proving equivalences between functions in a functional language often requires
structural induction.

Among the methods of proving this class of properties there are methods that can be applied not only
for proving properties, but also for program transformation.
One of these methods is supercompilation\optcite{Turchin1986Supercompiler}. Supercompilation
consists in transforming a program into an equivalent one by using rewriting rules (such as driving
and generalization) together with folding. Supercompilation is often used for program optimization,
but it can also be used for other problems, in particular for proving equivalence between functions
(to this end both functions are supercompiled and then their results are syntactically
compared\optcite{Lisitsa2008EqTesting,Klyuchnikov2009Proving}). One of the variation of
supercompilation is multi-result supercompilation\optcite{Klyuchnikov2011Multi1} which can produce a
set of programs instead of a single one. One of the advantages of multi-result supercompilation is
the possibility of using more precise methods to choose the best program from the resulting set of
programs instead of using local heuristics which control the process of rewriting.

Another method which can be applied both to the problem of proving equivalence and to the problem of
program transformation is equality saturation which was introduced in the paper ``Equality
Saturation: A New Approach to Optimization''by Tate et al.\footfullcite{Tate2009EqSaturation}.
Equality saturation consists in applying transformations to a data structure that compactly
represents a set of equivalent programs. In equality saturation every transformation is applied
non-destructively, i.e.\ the original program is retained in the set. To decrease memory consumption
equivalent parts of different programs in the set are shared. In the end a single program may be
chosen from the set (this step is not required if the goal was to prove equivalence).

Thus, equality saturation and multi-result supercompilation have the same motivation, however
the compact representation of sets of programs used in equality saturation hasn't been used in
multi-result supercompilation so far (in previous works on multi-result supercompilation some less
powerful tricks were used to decrease memory consumption). On the other hand, equality saturation
was used for transforming imperative languages whereas supercompilation is designed primarily for
functional languages. For these reasons it seems promising to research into combining equality
saturation and supercompilation and applying the resulting combined method to the task of proving
properties of functional programs.

% Таким образом задача индуктивного доказательства эквивалентности программ на нестрогих 
% функциональных языках является актуальной, а также актуальным является исследование методов,
% работающих ``в ширину'', в частности, насыщения равенствами. Поэтому исследование применения
% методов насыщения равенствами к задаче доказательства эквивалентности функциональных программ
% представляется актуальным.

% У последних часто отсутствует индукция --- здесь есть два варианта: использовать
% такие пруверы для доказательства базы и шага индукции либо применять индукцию как специальное 
% преобразование внутри прувера. Последний подход исследован плохо, поэтому мы пошли таким путём.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection*{The goal and tasks of the work}

The goal of this work is to research the possibility of applying a combination of equality
saturation and multi-result supercompilation to the task of discovering and proving properties of
programs in a non-strict first-order functional programming language.

The tasks are as follows:
\begin{itemize}
  \item Develop a compact representation of sets of functional programs.
  \item Develop a method of transforming this compact representation based on both equality
    saturation and supercompilation.
  \item Develop a method of proving equivalence between functions based on the developed method of
    transformation, such that it allows proofs by induction and coinduction.
  \item Implement the developed method in an experimental tool for proving equivalence of functions
    and test it on a suite of problems.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection*{Scientific novelty}

The original work on equality saturation by Tate et al.\ considered optimization of imperative
programs, however during the process of transformation they were represented in a functional form.
In our work we consider a functional language from the beginning, in particular this means that we
have to put less restrictions on the recursion: in imperative languages loops are widely used
instead of recursion, and considering only loops is enough for most imperative programs, whereas for
functional languages full support of recursion is expected.

In the work of Tate et al.\ sets of programs are represented with a data structure called E-PEG (PEG
stands for Program Expression Graph), which is based on E-graphs (graphs, whose nodes are split into
equivalence classes). In our work to represent sets of functional programs we introduce the notion
of polyprogram, which is analogous to E-PEG, but conceptually simpler: informally a polyprogram is a
program that may contain multiple definitions of the same function. The main difference between
polyprograms and E-PEGs is that polyprograms work with functions, each with its own set of
parameters, whereas E-PEGs work with values, and the input parameters are shared for the whole
E-PEG.

Moreover, we eliminate duplication of functions which differ only in the order of their parameters.
For E-graph-like structures this has never been done before, the closest previous result is
elimination of duplication of nodes representing values which differ by some integer
value\footfullcite{Nieuwenhuis2003CCwithInt}.

We also introduce a special transformation which we call merging by bisimulation. It works similarly
to proving equivalence by induction or coinduction. In the work of Tate et al.\ a similar but less
powerful transformation was used, which was able to merge isomorphic loops. Merging by bisimulation
also plays a role close to discovering and proving lemmas in multi-level
supercompilation\optcite{Klyuchnikov2010HLSC}.

% днако для обеспечения корректности в данной
% диссертации используются признаки структурной и защищённой рекурсии вместо теории улучшения Сэндса.

% Для выделения наилучшей программы при использовании насыщения равенствами для оптимизации в 
% \footfullcite{Tate2009EqSaturation} использовался подход, основанный
% на статической оценке времени работы программы как суммы весов входящих в неё конструкций. В данной
% диссертационной работе используется другой подход, основанный на запуске полипрограммы на
% тестовых данных. Этот подход эквивалентен запуску на тестовых данных всех программ, представленных
% полипрограммой, с последующим выбором наилучшей, но при этом гораздо быстрее.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \subsection*{Practical importance of the work}

% Настоящая работа показывает, что на основе метода насыщения равенствами и преобразований из
% суперкомпиляции можно построить систему преобразования функциональных программ, применимую для
% доказательства эквивалентности.

% Показано, что при этом удаётся избежать проблемы, связанной с комбинаторным взрывом количества
% функций, отличающихся только порядком аргументов. 

% Также было показано, что можно сформулировать принцип индукции как специальное преобразование,
% работающее в рамках системы насыщения. Полученный метод можно успешно применять для доказательства
% эквивалентности функций, что продемонстрировано тестированием на наборе примеров и сравнением
% с аналогичными инструментами.

% Было также продемонстрировано, что при применении метода для оптимизации возможно использовать
% запуск полипрограммы на тестовом наборе данных для выбора наиболее быстрой программы.

% Кроме того, была показана связь методов насыщения равенствами и суперкомпиляции.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{Results}

The following results have been accomplished in this work:
\begin{itemize}
  \item A new method of transforming sets of first-order functional programs has been developed
    based on equality saturation and supercompilation.
    \begin{itemize}
      \item The notion of a polyprogram has been proposed for representing sets of functional
        programs. A semantics of polyprograms has been defined.
      \item A set of local polyprogram transformation rules has been developed.
      \item A global transformation called merging by bisimulation has been proposed for proving
        equivalence of functions by induction and coinduction.
    \end{itemize}
  \item The developed method has been implemented in a system for proving equivalence of functions
    in a non-strict first-order functional language.
  \item The implemented system has been tested on a set of sample problems.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection*{Publications of the author on the topic of the thesis}
% Также были опубликованы следующие статьи, включая две в научных журналах из списка
% ВАК:
\begin{enumerate}
  \item \fullcite{AltGrechanik2012Overgraph}
  \item \fullcite{AltGrechanik2013SupercompilationHypergraph}
  % \item \fullcite{GrechanikKlyuchnikovRomanenko2013Staged}
  % \item \fullcite{StagedMRSC}
  \item \fullcite{AltGrechanik14proceedings}
  \item \fullcite{AltGrechanikMeta2014}
  \item (In Russian) \fullcite{AltGrechanik15Ru}
  \item \fullcite{AltGrechanik15journal}
  \item (In Russian) \fullcite{AltGrechanik2017PolyprogramsRu}
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

