((latex-mode .
  ((eval . (set (make-local-variable 'my-project-path)
                (file-name-directory
                 (let ((d (dir-locals-find-file ".")))
                   (if (stringp d) d (car d))))))
   (eval . (setq TeX-master (expand-file-name "grechanik-slides.tex" my-project-path)))
   (eval . (setq bibtex-completion-bibliography
                 (expand-file-name "../src/references.bib" my-project-path))))))
